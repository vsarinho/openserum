
<?php
//Aqui importamos todas as classes que poderão ser usadas baseado nas solicitações que forem feitas.
require_once("../model/class/monitor.Class.php"); // Classe Bean
require_once("../model/metodos/monitor.Metodos.php"); // Classe metodos
require_once("../view/monitor.View.php"); //Classe View

//Armazena na variável $acao o que o sistema esta requisitando (cadastrar, autenticar, excluir, etc)
$acao = $_REQUEST["acao"];
//Baseado no que foi solicitado, chama na classe Metodo o método responsável por tal tarefa, e depois manda pra View a resposta, para ser exibida de alguma forma ao usuário
switch($acao){
	
	case "cadastrar":{
		$monitor = new monitor();
		$monitorDAO = new monitorMetodos();
		$monitorView = new monitorViews();
			
		$monitor->setidprocesso($_REQUEST["ip"]);	
		$monitor->setmonitor($_REQUEST["monitor"]);	
		$monitor->setpaciente($_REQUEST["paciente"]);	
		$monitor->setcelular($_REQUEST["celular"]);	

		$resultado = $monitorDAO->monitorCadastra($monitor);
		$monitorView->monitorViewCadastra($resultado);
		
	}break;
//--------------------------------------------
	case "exibir":{
		
		$monitor = new monitor();
		$monitorDAO = new monitorMetodos();
		$monitorView = new monitorViews();

		$monitor->setid($_REQUEST["id"]);

		$resultado = $monitorDAO->monitorExibir($monitor);
		$monitorView->monitorViewExibir($resultado);
		
	}break;
//----------------------------------------------------------------------	
	case "pesquisa":{
		$monitor = new monitor();
		$monitorDAO = new monitorMetodos();
		$monitorView = new monitorViews();
		
		$monitor->setid($_REQUEST["ip"]);	
		$monitor->setmonitor($_REQUEST["monitor"]);	
		$monitor->setpaciente($_REQUEST["paciente"]);	
		$monitor->setcelular($_REQUEST["celular"]);	

		$resultado = $monitorDAO->monitorPesquisa($monitor);
		$monitorView->monitorViewPesquisa($resultado);
		
	}break;
//--------------------------------------------
	case "excluir":{
		
		$monitor = new monitor();
		$monitorDAO = new monitorMetodos();
		$monitorView = new monitorViews();
			
		$monitor->setid($_REQUEST["id"]);
		$resultado = $monitorDAO->monitorExcluir($monitor);
		$monitorView->monitorViewExcluir($resultado);
		
	}break;
	
	
	case "editar":{
		$monitor = new monitor();
		$monitorDAO = new monitorMetodos();
		$monitorView = new monitorViews();

		$monitor->setid($_REQUEST["id"]);		
		$monitor->setip($_REQUEST["ip"]);	
		$monitor->setmonitor($_REQUEST["monitor"]);	
		$monitor->setpaciente($_REQUEST["paciente"]);	
		$monitor->setcelular($_REQUEST["celular"]);	
		
		$resultado = $monitorDAO->monitorEditar($monitor);
		$monitorView->monitorViewEditar($resultado);
		
	}break;
//--------------------------------------------
	case "verifica_status":{
		$monitor = new monitor();
		$monitorDAO = new monitorMetodos();
		$monitorView = new monitorViews();

	//	$monitor->setmo($_REQUEST["id"]);		
		$resultado = $monitorDAO->monitorStatus($monitor);
		$monitorView->monitorViewStatus($resultado);
		
	}break;
//--------------------------------------------	
	default:
		return null; //Por padrão, esse switch não retorna nada.
		
}


?>

