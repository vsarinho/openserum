﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head><?php


class monitorViews{

	function monitorViewCadastra($resposta)
	{
	
		/*Se a variável $resposta estiver neste momento como TRUE, então os dados estão corretos e podemos 
		exibir uma mensagem de sucesso. Caso contrário, irá cair no else, que irá alertar que os dados são inválidos.*/
		if($resposta<>"")
		{

			include("../monitorList.php");
	
		}
		else
		{
			echo '<p class="red">Registro <strong>'.$resposta.'</strong> Não Inserido, falha no processo! </p>';
		}
		
		exit;
		
		
	}
//---------------------------------------------------------------
	
	function monitorViewExcluir($resposta)
	{
	
		/*Se a variável $resposta estiver neste momento como TRUE, então os dados estão corretos e podemos 
		exibir uma mensagem de sucesso. Caso contrário, irá cair no else, que irá alertar que os dados são inválidos.*/
		if($resposta<>"")
		{
			
			echo '<p class="blue">Registro <strong>'.$resposta.'</strong> Cancelado com sucesso! </p>';

		}
		else
		{
			
			echo '<p class="red">Registro <strong>'.$resposta.'</strong> Não cancelado, falha no processo! </p>';	
						
		}
		exit;
	}	

//---------------------------------------------------------------

	function monitorViewExibir($monitorObj)
	{
	
		/*Se a variável $resposta estiver neste momento como TRUE, então os dados estão corretos e podemos 
		exibir uma mensagem de sucesso. Caso contrário, irá cair no else, que irá alertar que os dados são inválidos.*/
		

	
		$ip=$monitorObj->getip();	
		$paciente=$monitorObj->getpaciente();	
		$monitor=$monitorObj->getmonitor();	
		$celular=$monitorObj->getcelular();	
		
		?>
		
          <div class="campos"> IP<br />
	     <input type="text" name="ip"    onkeypress="limpaResultado()" value="<? echo $ip?>" id="ip" />
	   </div>
       
       <div class="campos"> MONITOR:<br />
          <input type="text" name="monitor"    onkeypress="limpaResultado()" value="<? echo $monitor?>" id="monitor" />
      </div>
      
      
      <div class="campos"> PACIENTE<br />
          <input type="text" name="paciente"    onkeypress="limpaResultado()" value="<? echo $paciente?>" id="paciente" />
	    </div>
             
             
	   <div class="campos"> CELULAR<br />
          <input type="text" name="telefone"    onkeypress="limpaResultado()" value="<? echo $celular?>" id="celular" />
	    </div>
		
		<?
		exit;
}	

//---------------------------------------------------------------

function monitorViewEditar($monitorObj)
	{
	

		$ip=$monitorObj->getip();	
		$paciente=$monitorObj->getpaciente();	
		$monitor=$monitorObj->getmonitor();	
		$celular=$monitorObj->getcelular();	
		
		?>
		
          <div class="campos"> IP<br />
	     <input type="text" name="ip"    onkeypress="limpaResultado()" value="<? echo $ip?>" id="ip" />
	   </div>
       
       <div class="campos"> MONITOR:<br />
          <input type="text" name="monitor"    onkeypress="limpaResultado()" value="<? echo $monitor?>" id="monitor" />
      </div>
      
      
      <div class="campos"> PACIENTE<br />
          <input type="text" name="paciente"    onkeypress="limpaResultado()" value="<? echo $paciente?>" id="paciente" />
	    </div>
             
             
	   <div class="campos"> CELULAR<br />
          <input type="text" name="telefone"     onkeypress="limpaResultado()" value="<? echo $celular?>" id="celular" />
	    </div>

		<?
		exit;
	}	
	
//---------------------------------------------------------------
	
function monitorViewStatus($sql)
		{
	
		/*Se a variável $resposta estiver neste momento como TRUE, então os dados estão corretos e podemos 
		exibir uma mensagem de sucesso. Caso contrário, irá cair no else, que irá alertar que os dados são inválidos.*/
	?>
<script src="js/load_paginas.js" type="text/javascript"></script>
<table width="98%" align="center">
  <tr >
  
		<td class="coluna_lista_fundo">MONITOR</td>
        <td class="coluna_lista_fundo">PACIENTE</td>
        <td class="coluna_lista_fundo">TELEFONE</td>
        <td class="coluna_lista_fundo">IP</td>
        <td class="coluna_lista_fundo">STATUS</td>
        <td class="coluna_lista_fundo">EDITE</td>
  </tr>
  <?php 
  	$c=0;
  	while($dados=mysql_fetch_array($sql))
	{
		$status=$dados["status"];

		
	?>
      
      
  <tr class="tabela_linha" bgcolor="<?php echo $cor;?>">
  
		<td class="coluna_texto" style="background-color:#FFF"  ><?php echo $dados["monitor"];?></td>
		<td class="coluna_texto" style="background-color:#FFF"><?php echo $dados["paciente"];?></td>
		<td class="coluna_texto" style="background-color:#FFF"><?php echo $dados["celular"];?></td>
		<td class="coluna_texto" style="background-color:#FFF"><?php echo $dados["ip"];?></td>
		<td class="coluna_texto" style="background-color:#FFF"><img src="images/<?php echo $status?>.png" width="20" height="20"/></td>
		<td class="coluna_texto" style="background-color:#FFF"><a href="#pagina-4" target="_self" id="includes/monitorForm?<?php echo $dados["monitor"];?>?Editar" class="link-ajax"><img src="images/edit.png" width="20" height="20"/></a></td>
  </tr>
  <?php 
  }?>
</table>

		
		<?
		exit;
	}	

}

?>
