#include <SPI.h>
#include <Ethernet.h>

//Constante que representa o pino onde o positivo do buzzer será ligado.
const int buzzer = 5;

int sensorPin = A0;      //Pino analógico em que o sensor está conectado.
int valorSensor = 0; //Usada para ler o valor do sensor em tempo real.
int tempo=0;
//dados da conexão ethernet
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,10);         //Define o endereço IP
char server[] = "192.168.1.4"; 

EthernetClient client;

//Método setup, executado uma vez ao ligar o Arduino.
void setup() {
  
   if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip);
  }
  
  //INICIA A SERIAL
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  
  //Inicia o Buzzer
  pinMode(buzzer,OUTPUT);
}


//Método loop, executado enquanto o Arduino estiver ligado.
void loop() {  
   //Lendo o valor do sensor.
   int valorSensor = analogRead(sensorPin);
   Serial.println(valorSensor);
  delay(100); 
  
  if(valorSensor<=1020)
  {
      tempo=tempo+100;
      
      if(tempo==10000)
      {
            Serial.println("Conectando");
            Serial.println(Ethernet.localIP());
            
           tone(buzzer,1500);   
           delay(100);    
      //Desligando o buzzer.
          noTone(buzzer);
          delay(500);
          
          tone(buzzer,1500);   
          delay(100);    
      //Desligando o buzzer.
          noTone(buzzer);
          delay(500);
            
          if (client.connect(server, 80)) 
           {
              delay(1000);
            Serial.println("Enviando Status de Inatividade");
            delay(2000);
            // Make a HTTP request:
            client.println("GET /especializacao/monitorsoro/registra_status.php?status=0");
            client.println("Host: 192.168.1.2");
            client.println("Connection: close");
            client.println();
            client.stop();
            delay(2000);
           }
          else 
          {
            // kf you didn't get a connection to the server:
            Serial.println("Falha na conexão");
            delay(1000);
          }
          tempo=0;
      }
 
  }  
  else
  {
     tone(buzzer,1500);   
     delay(300);    
      //Desligando o buzzer.
      noTone(buzzer);
      delay(1500); 
      
      

      
      //Envia dados para o servidor
    //  Ethernet.begin(mac);
      
      Serial.println("Conectando");
      Serial.println(Ethernet.localIP());
            
      if (client.connect(server, 80)) 
       {
          delay(1000);
        Serial.println("Enviando Status Atividade");
        delay(2000);
        // Make a HTTP request:
        client.println("GET /especializacao/monitorsoro/registra_status.php?status=1");
        client.println("Host: 192.168.1.2");
        client.println("Connection: close");
        client.println();
        client.stop();
        delay(2000);
       }
      else 
      {
        // kf you didn't get a connection to the server:
        Serial.println("Falha na conexão");
        delay(1000);
      }
      tempo=0;
      
  }
}
